# Copyright (c) 2022, Linaro Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import json

from PySide2.QtCore import QAbstractItemModel, QModelIndex, Qt

class IGTCounter:
    def __init__(self, string = None):
        self.passed = 0
        self.warn = 0
        self.fail = 0
        self.skip = 0
        self.dmesg = False
        if not string:
            return
        if string == 'pass':
            self.passed = 1
        elif string == 'skip':
            self.skip = 1
        elif string == 'warn':
            self.warn = 1
        elif string == 'dmesg-warn':
            self.warn = 1
            self.dmesg = True
        elif string == 'fail':
            self.fail = 1
        elif string == 'dmesg-fail':
            self.fail = 1
            self.dmesg = True
        elif string == 'incomplete':
            self.fail = 1 # incomplete in final results is failure in most of cases
        else:
            raise RuntimeError("Unsupported status '%s'" % string)

    def add_all(self, items):
        for item in items:
            self.passed += item.passed
            self.skip += item.skip
            self.warn += item.warn
            self.fail += item.fail

        return self

    def __str__(self):
        return "%d (%d with warn) / %d / %d" % (self.passed, self.warn, self.fail, self.skip)

class IGTResultsItem:
    def __init__(self, name, parent = None, values = {}):
        self.name = name
        self._values = values
        self._result = None
        if 'result' in values:
            self._result = IGTCounter(values['result'])
        self._children = {}
        self._parent = parent

    def columnCount(self):
        return 3

    def childCount(self):
        return len(self._children)

    def child(self, n):
        for i, val in enumerate(self._children.values()):
            if i == n:
                return val

        raise IndexError("dictionary index out of range")

    def row(self):
        if not self._parent:
            return 0

        for i, val in enumerate(self._parent._children.values()):
            if val == self:
                return i

    def add(self, path, v):
        step = path[0]
        if len(path) == 1:
            self._children[step] = IGTResultsItem(step, self, v)
            return

        if not step in self._children:
            self._children[step] = IGTResultsItem(step, self)
        self._children[step].add(path[1:], v)

    def parent(self):
        return self._parent

    def _get_counter(self):
        if self._result:
            return self._result
        return IGTCounter().add_all(map(lambda i: i._get_counter(), self._children.values()))

    def data(self, column):
        if column == 0:
            return self.name
        elif column == 1:
            if 'result' in self._values:
                return self._values['result']
            else:
                return str(self._get_counter())
        elif column == 2:
            if 'err' in self._values:
                return self._values['err']
        return None


class IGTResultsModel(QAbstractItemModel):
    def __init__(self, name):
        super().__init__()

        self._root = IGTResultsItem("")

        with open(name, "r") as file:
            data = json.load(file)
            for (k, v) in data['tests'].items():
                self._root.add(k.split("@"), v)

    def index(self, row, column, _parent=QModelIndex()):
        if not _parent or not _parent.isValid():
            parent = self._root
        else:
            parent = _parent.internalPointer()

        if not QAbstractItemModel.hasIndex(self, row, column, _parent):
            return QModelIndex()

        child = parent.child(row)
        if child:
            return QAbstractItemModel.createIndex(self, row, column, child)
        else:
            return QModelIndex()

    def parent(self, index):
        if index.isValid():
            p = index.internalPointer().parent()
            if p:
                return QAbstractItemModel.createIndex(self, p.row(), 0, p)
        return QModelIndex()

    def columnCount(self, index):
        if index.isValid():
            return index.internalPointer().columnCount()
        return self._root.columnCount()

    def rowCount(self, index):
        if index.isValid():
            return index.internalPointer().childCount()
        return self._root.childCount()

    def data(self, index, role):
        if not index.isValid():
            return None
        node = index.internalPointer()
        if role == Qt.DisplayRole:
            return node.data(index.column())
        return None

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation != Qt.Horizontal or role != Qt.DisplayRole:
            return None

        if section == 0:
            return "Name"
        elif section == 1:
            return "Result"
        elif section == 2:
            return "Message"
