# Copyright (c) 2022, Linaro Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys

from PySide2.QtWidgets import QApplication
from PySide2.QtWidgets import QMainWindow, QTreeView

class MainWindow(QMainWindow):
    def __init__(self, model):
        super().__init__()

        self.setWindowTitle("IGT results")

        self.tree = QTreeView()
        self.tree.setModel(model)
        self.tree.resizeColumnToContents(0)
        self.tree.resizeColumnToContents(1)

        self.tree.expanded.connect(self.resize_column)
        self.tree.collapsed.connect(self.resize_column)

        self.tree.setAlternatingRowColors(True)

        self.setCentralWidget(self.tree)

    def resize_column(self):
        self.tree.resizeColumnToContents(0)

from model import IGTResultsModel

app = QApplication(sys.argv)

model = IGTResultsModel(app.arguments()[1])

window = MainWindow(model)
window.showMaximized()

sys.exit(app.exec_())
